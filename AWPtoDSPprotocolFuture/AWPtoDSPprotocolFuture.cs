﻿using System;
using System.Collections.Generic;
using System.Collections.Concurrent;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.Threading;

using llcss;

using Nito.AsyncEx;

using Protocols;
using DspDataModel.Server; // RequestResult*
using DspDataModel.Tasks; // DspServerMode
using DspDataModel.Hardware; // ReceiversChannel
using DspDataModel.LinkedStation; // StationRole
using System.Text.RegularExpressions;

public class AWPtoDSPprotocolFuture
{
    private TcpClient client;

    private readonly AsyncLock asyncLock;

    private readonly ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>> concurrentDictionary;

    private int _AwaiterTime = 400;
    public int AwaiterTime
    {
        get { return _AwaiterTime; }
        set
        {
            if (_AwaiterTime != value)
                _AwaiterTime = value;
        }
    }

    double GlobalDotsPerMHz = 9831.0;
    double GlobalBandWidthMHz = 30.0;

    private string IP;
    private int port;

    bool disconnect = false;

    private RangeSector[] SaveSectorsAndRanges;
    private int SaveThreshold;
    private double SaveSD;
    private double SaveBandwidth;
    private int SaveDuration;
    private DspServerMode SaveMode;

    private FRSJammingSetting[] SaveFRSJammingSettings;

    public delegate void IsConnectedEventHandler(bool isConnected);
    public event IsConnectedEventHandler IsConnected;

    public delegate void OnReadEventHandler(bool isRead);
    public event OnReadEventHandler IsRead;

    public delegate void IsWriteEventHandler(bool isWrite);
    public event IsWriteEventHandler IsWrite;



    public AWPtoDSPprotocolFuture()
    {
        asyncLock = new AsyncLock();
        concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        //AppDomain.CurrentDomain.FirstChanceException += CurrentDomain_FirstChanceException;
    }

    //void CurrentDomain_FirstChanceException(object sender, System.Runtime.ExceptionServices.FirstChanceExceptionEventArgs e)
    //{
    //}

    /*public AWPtoBearingDSPprotocolNew(string IP, int port)
    {
        //client = new TcpClient();
        asyncLock = new AsyncLock();
        concurrentDictionary = new ConcurrentDictionary<int, ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>>();
        this.IP = IP;
        this.port = port;
    }*/

    static byte OwnServerAddress = 0;
    static MessageHeader GetMessageHeader(int code, int length)
    {
        //return new MessageHeader(0, 1, (byte)code, 0, length);
        return new MessageHeader(OwnServerAddress, 1, (byte)code, 0, length);
    }

    public async Task ConnectToBearingDSP(string IP, int port)
    {
        disconnect = false;
        client = new TcpClient();
        while (!client.Connected)
        {
            try
            {
                await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                this.IP = IP;
                this.port = port;
            }
            catch (Exception)
            {

            }
            if (!client.Connected)
                await Task.Delay(500);
        }

        await OnConnected();

        Task.Run(() => ReadThread());

        //await OnReconnected();

        if (IsConnected != null)
            IsConnected(true);
    }

    public void ConnectToBearingDSPSync(string IP, int port)
    {
        Task.Run(async () => await ConnectToBearingDSP(IP, port)).GetAwaiter();
    }

    private async Task ConnectToServerWCancelToken(string IP, int port, CancellationToken token)
    {
        disconnect = false;
        client = new TcpClient();

        while (!client.Connected)
        {
            if (token.IsCancellationRequested)
                return;
            try
            {
                await client.ConnectAsync(IPAddress.Parse(IP), port).ConfigureAwait(false);

                this.IP = IP;
                this.port = port;
            }
            catch (Exception)
            {

            }
            if (!client.Connected)
                await Task.Delay(500);
        }

        Task.Run(() => ReadThread());

        if (IsConnected != null)
            IsConnected(true);
    }

    public async Task<string> IQConnectToServer(string IP, int port, int timeout = 3000)
    {
        Regex regex = new Regex(@"^(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])(\.(25[0-5]|2[0-4][0-9]|[0-1][0-9]{2}|[0-9]{2}|[0-9])){3}$");
        if (regex.IsMatch(IP, 0))
        {
            CancellationTokenSource cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;

            var task = ConnectToServerWCancelToken(IP, port, token);

            if (await Task.WhenAny(task, Task.Delay(timeout)) == task)
            {
                Console.WriteLine("task completed within timeout");
                return "OK";
            }
            else
            {
                cts.Cancel();
                Console.WriteLine("timeout logic");
                return "Invalid IP address or Server not found";
            }
        }
        else
        {
            return "Invalid IP address";
        }
    }

    public void DisconnectFromBearingDSP()
    {
        disconnect = true;
        //using (client = new TcpClient())
        //{

        //}
        if (client != null)
            client.Close();
        if (IsConnected != null)
            IsConnected(false);
    }

    private async Task OnConnected()
    {
        var taskQueues = concurrentDictionary.Values.ToArray();
        foreach (var queue in taskQueues)
        {
            foreach (var task in queue)
            {
                //task.SetCanceled();
                task.SetResult(null);
            }
        }
        concurrentDictionary.Clear();
    }

    private async Task OnReconnected()
    {
        if (SaveSectorsAndRanges != null
            && SaveThreshold != null && SaveSD != null && SaveBandwidth != null && SaveDuration != null
            && SaveMode != null)
        {
            var answer1 = await SetSectorsAndRanges(0, 0, SaveSectorsAndRanges);
            var answer2 = await SetFilters(SaveThreshold, SaveSD, SaveBandwidth, SaveDuration);
            var answer3 = await SetMode(SaveMode);
        }

        if (SaveFRSJammingSettings != null)
        {
            var answer4 = await SetFRSJamming(DspDataModel.TargetStation.Current, SaveFRSJammingSettings);
        }

    }

    private async Task ReadThread()
    {
        while (client.Connected)
        {
            var headerBuffer = new byte[MessageHeader.BinarySize];
            var header = new MessageHeader();
            var count = 0;

            try
            {
                count = await client.GetStream().ReadAsync(headerBuffer, 0, MessageHeader.BinarySize).ConfigureAwait(false);
                if (IsRead != null)
                    IsRead(true);
            }
            catch (Exception)
            {
                //Console.WriteLine("Сервер отвалился");
                if (IsRead != null)
                    IsRead(false);
                if (IsConnected != null)
                    IsConnected(false);
                if (disconnect == false)
                    Task.Run(() => ConnectToBearingDSP(IP, port));
                //break;
            }
            if (count != MessageHeader.BinarySize)
            {
                //fatal error
                //break;
                if (IsRead != null)
                    IsRead(false);
                return;
            }

            var b00l = MessageHeader.TryParse(headerBuffer, out header);
            if (b00l == false) Console.WriteLine("AWP: Critical Error: .TryParse");

            switch (header.Code)
            {
                case 1:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 2:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 3:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 4:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 6:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 7:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 8:
                    await HandleResponse<GetAttenuatorsResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 9:
                    await HandleResponse<GetAmplifiersResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 11:
                    await HandleResponse<GetRadioSourcesResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 12:
                    await HandleResponse<GetSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 16:
                    await HandleResponse<ExecutiveDFResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 17:
                    await HandleResponse<QuasiSimultaneouslyDFResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 18:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 19:
                    await HandleResponse<TechAppSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 20:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 21:
                    await HandleResponse<HeterodyneRadioSourcesResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 22:
                    await HandleResponse<GetCalibrationProgressResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 23:
                    await HandleResponse<GetBandAmplitudeLevelsResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 24:
                    await HandleResponse<GetAmplitudeTimeSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 25:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 26:
                    await HandleResponse<StopRecordingResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 27:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 29:
                    await HandleEvent<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 30:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 31:
                    await HandleResponse<GetAdaptiveThresholdResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 32:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 33:
                    await HandleEvent<RadioJamStateUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 34:
                    await HandleResponse<GetFhssNetworksResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 35:
                    await HandleResponse<SpecialFrequenciesMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 36:
                    await HandleResponse<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 37:
                    await HandleResponse<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 38:
                    await HandleResponse<SectorsAndRangesMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 39:
                    await HandleResponse<FrsJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 40:
                    await HandleResponse<FhssJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 41:
                    await HandleResponse<GetScanSpeedResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 42:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 43:
                    await HandleResponse<StationLocationMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 44:
                    await HandleResponse<GetRadioControlSpectrumResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 45:
                    await HandleEvent<ShaperStateUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 46:
                    await HandleEvent<FhssRadioJamUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 47:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 48:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 49:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 50:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 51:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 52:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 53:
                    await HandleResponse<GetBearingPanoramaSignalsResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 54:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 55:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 56:
                    await HandleEvent<MasterSlaveStateChangedEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 57:
                    await HandleResponse<DirectionCorrectionMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 58:
                    await HandleResponse<SearchFhssMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 59:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 60:
                    await HandleResponse<MasterSlaveStateChangedEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 61:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 62:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 63:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 64:
                    await HandleEvent<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 65:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 66:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 67:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 68:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 70:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 71:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 72:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 73:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 74:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 75:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 76:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 77:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 78:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 79:
                    await HandleResponse<GetBandsAdaptiveThresholdResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 80:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 81:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 82:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 83:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 84:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 85:
                    await HandleResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 101:
                    await HandleEvent<SpecialFrequenciesMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 118:
                    await HandleEvent<FiltersMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 102:
                    await HandleEvent<ModeMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 103:
                    await HandleEvent<SectorsAndRangesMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 104:
                    await HandleEvent<AttenuatorsMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 106:
                    await HandleEvent<FrsJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 107:
                    await HandleEvent<FhssJammingMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 127:
                    await HandleEvent<DirectionCorrectionMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 142:
                    await HandleEvent<StationLocationMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 150:
                    await HandleEvent<SendTextMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 151:
                    await HandleEvent<SetTimeRequest>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 152:
                    await HandleEvent<StorageActionMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 159:
                    await HandleEvent<SearchFhssMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 161:
                    await HandleEvent<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 162:
                    await HandleEvent<GetRadioSourcesResponse>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 163:
                    await HandleEvent<AntennaDirectionsMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 166:
                    await HandleEvent<NumberOfSatellitesUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 170:
                    await HandleEvent<ShaperConditionUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 171:
                    await HandleEvent<ShaperVoltageUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 172:
                    await HandleEvent<ShaperPowerUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 173:
                    await HandleEvent<ShaperConditionUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                case 174:
                    await HandleEvent<ShaperConditionUpdateEvent>(header, headerBuffer).ConfigureAwait(false);
                    break;
                default:
                    Console.WriteLine("AWP: Unknown Code: " + header.Code);
                    await ReadResponse<DefaultMessage>(header, headerBuffer).ConfigureAwait(false);
                    break;
            }
        }
    }


    private async Task HandleResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
    {
        var response = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);
        var taskCompletionSource = new TaskCompletionSource<IBinarySerializable>();
        if (!concurrentDictionary[header.Code].TryDequeue(out taskCompletionSource))
        {
            throw new Exception("Response came, but no one is waiting for it");
        }
        if (response != null)
        {
            taskCompletionSource.SetResult(response);
        }
        else
        {
            taskCompletionSource.SetResult(null);
            //taskCompletionSource.SetException(new Exception("Can't execute request"));
        }
    }

    private async Task<T> ReadResponse<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
    {
        var count = 0;
        var added = IsShaperCommand() ? 3 : 0;
        var buffer = new byte[MessageHeader.BinarySize + header.InformationLength + added];
        headerBuffer.CopyTo(buffer, 0);

        if (header.InformationLength != 0)
        {
            var difference = header.InformationLength + added;

            var shiftpos = 0;

            try
            {
                while (difference != 0)
                {
                    count = await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize + shiftpos, difference).ConfigureAwait(false);
                    if (IsRead != null)
                        IsRead(true);
                    shiftpos += count;
                    difference = difference - count;
                }
            }
            catch { return null; }
        }

        //if (await client.GetStream().ReadAsync(buffer, MessageHeader.BinarySize, header.InformationLength).ConfigureAwait(false) != header.InformationLength)
        //{
        //    return null;
        //}
        try
        {
            var result = new T();
            result.Decode(buffer, 0);
            return result;
        }
        catch (Exception e)
        {
            var с = e.ToString();
            return null;
        }

        bool IsShaperCommand() => 
            header.Code == 170 ||
            header.Code == 173 ||
            header.Code == 174;
    }


    //Events


    //Шифр 29
    public delegate void GetSettingsUpdateEventHandler(ModeMessage answer);
    public event GetSettingsUpdateEventHandler GetSettingsUpdate;

    //Шифр 33
    public delegate void RadioJamStateUpdateEventHandler(RadioJamStateUpdateEvent answer);
    public event RadioJamStateUpdateEventHandler RadioJamStateUpdate;

    //Шифр 45
    public delegate void ShaperStateUpdateEventHandler(ShaperStateUpdateEvent answer);
    public event ShaperStateUpdateEventHandler ShaperStateUpdate;

    //Шифр 46
    public delegate void FhssRadioJamUpdateEventHandler(FhssRadioJamUpdateEvent answer);
    public event FhssRadioJamUpdateEventHandler FhssRadioJamUpdate;

    //Шифр 56
    public delegate void MasterSlaveStateChangedEventHandler(MasterSlaveStateChangedEvent answer);
    public event MasterSlaveStateChangedEventHandler MasterSlaveStateChangedUpdate;

    //Шифр 64
    public delegate void FormShaperEventHandler(DefaultMessage answer);
    public event FormShaperEventHandler FormShaperUpdate;

    //Шифр 101
    public delegate void SpecialFrequenciesMessageEventHandler(SpecialFrequenciesMessage answer);
    public event SpecialFrequenciesMessageEventHandler SpecialFrequenciesMessageUpdate;

    //Шифр 118
    public delegate void FiltersMessageEventHandler(FiltersMessage answer);
    public event FiltersMessageEventHandler FiltersMessageUpdate;

    //Шифр 102
    public delegate void ModeMessageEventHandler(ModeMessage answer);
    public event ModeMessageEventHandler ModeMessageUpdate;

    //Шифр 103
    public delegate void SectorsAndRangesMessageEventHandler(SectorsAndRangesMessage answer);
    public event SectorsAndRangesMessageEventHandler SectorsAndRangesMessageUpdate;

    //Шифр 104
    public delegate void AttenuatorsMessageEventHandler(AttenuatorsMessage answer);
    public event AttenuatorsMessageEventHandler AttenuatorsMessageUpdate;

    //Шифр 106
    public delegate void FrsJammingMessageEventHandler(FrsJammingMessage answer);
    public event FrsJammingMessageEventHandler FrsJammingMessageUpdate;

    //Шифр 107
    public delegate void FhssJammingMessageEventHandler(FhssJammingMessage answer);
    public event FhssJammingMessageEventHandler FhssJammingMessageUpdate;

    //Шифр 127
    public delegate void DirectionCorrectionMessageEventHandler(DirectionCorrectionMessage answer);
    public event DirectionCorrectionMessageEventHandler DirectionCorrectionMessageUpdate;

    //Шифр 142
    public delegate void StationLocationMessageEventHandler(StationLocationMessage answer);
    public event StationLocationMessageEventHandler StationLocationMessageUpdate;

    //Шифр 150
    public delegate void MasterSlaveSendTextMessageEventHandler(SendTextMessage answer);
    public event MasterSlaveSendTextMessageEventHandler MasterSlaveSendTextMessageUpdate;

    //Щифр 151
    public delegate void SetTimeEventHandler(SetTimeRequest answer);
    public event SetTimeEventHandler SetTimeUpdate;

    //Шифр 152
    public delegate void StorageActionMessageEventHandler(StorageActionMessage answer);
    public event StorageActionMessageEventHandler StorageActionMessageUpdate;

    //Шифр 159
    public delegate void SearchFHSSMessageEventHandler(SearchFhssMessage answer);
    public event SearchFHSSMessageEventHandler SearchFhssMessageUpdate;

    //Шифр 161
    public delegate void GetASPRadioSourcesEventHandler(DefaultMessage answer);
    public event GetASPRadioSourcesEventHandler GetASPRadioSourcesUpdate;

    //Шифр 162
    public delegate void PushASPRadioSourcesEventHandler(GetRadioSourcesResponse answer);
    public event PushASPRadioSourcesEventHandler PushASPRadioSourcesUpdate;

    //Шифр 163
    public delegate void AntennaDirectionsMessageEventHandler(AntennaDirectionsMessage answer);
    public event AntennaDirectionsMessageEventHandler AntennaDirectionsMessageUpdate;

    //Шифр 166
    public delegate void NumberOfSatellitesUpdateEventHandler(NumberOfSatellitesUpdateEvent answer);
    public event NumberOfSatellitesUpdateEventHandler NumberOfSatellitesUpdate;

    //Шифр 170
    public delegate void ShaperModeUpdateEventHandler(ShaperConditionUpdateEvent answer);
    public event ShaperModeUpdateEventHandler ShaperModeUpdate;

    //Шифр 171
    public delegate void ShaperVoltageUpdateEventHandler(ShaperVoltageUpdateEvent answer);
    public event ShaperVoltageUpdateEventHandler ShaperVoltageUpdate;

    //Шифр 172
    public delegate void ShaperPowerUpdateEventHandler(ShaperPowerUpdateEvent answer);
    public event ShaperPowerUpdateEventHandler ShaperPowerUpdate;

    //Шифр 173
    public delegate void ShaperTemperatureUpdateEventHandler(ShaperConditionUpdateEvent answer);
    public event ShaperTemperatureUpdateEventHandler ShaperTemperatureUpdate;

    //Шифр 174
    public delegate void ShaperAmperageUpdateEventHandler(ShaperConditionUpdateEvent answer);
    public event ShaperAmperageUpdateEventHandler ShaperAmperageUpdate;


    private async Task HandleEvent<T>(MessageHeader header, byte[] headerBuffer) where T : class, IBinarySerializable, new()
    {
        var responseEvent = await ReadResponse<T>(header, headerBuffer).ConfigureAwait(false);

        //Шифр 29
        if (responseEvent is ModeMessage)
        {
            var answer = responseEvent as ModeMessage;
            if (GetSettingsUpdate != null)
                GetSettingsUpdate(answer);
        }

        //Шифр 33
        if (responseEvent is RadioJamStateUpdateEvent)
        {
            var answer = responseEvent as RadioJamStateUpdateEvent;
            if (RadioJamStateUpdate != null)
                RadioJamStateUpdate(answer);
        }

        //Шифр 45
        if (responseEvent is ShaperStateUpdateEvent)
        {
            var answer = responseEvent as ShaperStateUpdateEvent;
            if (ShaperStateUpdate != null)
                ShaperStateUpdate(answer);
        }

        //Шифр 46
        if (responseEvent is FhssRadioJamUpdateEvent)
        {
            var answer = responseEvent as FhssRadioJamUpdateEvent;
            if (FhssRadioJamUpdate != null)
                FhssRadioJamUpdate(answer);
        }

        //Шифр 56
        if (responseEvent is MasterSlaveStateChangedEvent)
        {
            var answer = responseEvent as MasterSlaveStateChangedEvent;
            if (MasterSlaveStateChangedUpdate != null)
                MasterSlaveStateChangedUpdate(answer);
        }

        //Шифр 64
        if (responseEvent is DefaultMessage)
        {
            var answer = responseEvent as DefaultMessage;
            if (answer.Header.Code == 64)
                if (FormShaperUpdate != null)
                    FormShaperUpdate(answer);
        }

        //Шифр 101
        if (responseEvent is SpecialFrequenciesMessage)
        {
            var answer = responseEvent as SpecialFrequenciesMessage;
            if (SpecialFrequenciesMessageUpdate != null)
                SpecialFrequenciesMessageUpdate(answer);
        }

        //Шифр 118
        if (responseEvent is FiltersMessage)
        {
            var answer = responseEvent as FiltersMessage;
            if (FiltersMessageUpdate != null)
                FiltersMessageUpdate(answer);
        }

        //Шифр 102
        if (responseEvent is ModeMessage)
        {
            var answer = responseEvent as ModeMessage;
            if (ModeMessageUpdate != null)
                ModeMessageUpdate(answer);
        }

        //Шифр 103
        if (responseEvent is SectorsAndRangesMessage)
        {
            var answer = responseEvent as SectorsAndRangesMessage;
            if (SectorsAndRangesMessageUpdate != null)
                SectorsAndRangesMessageUpdate(answer);
        }

        //Шифр 104
        if (responseEvent is AttenuatorsMessage)
        {
            var answer = responseEvent as AttenuatorsMessage;
            if (AttenuatorsMessageUpdate != null)
                AttenuatorsMessageUpdate(answer);
        }

        //Шифр 106
        if (responseEvent is FrsJammingMessage)
        {
            var answer = responseEvent as FrsJammingMessage;
            if (FrsJammingMessageUpdate != null)
                FrsJammingMessageUpdate(answer);
        }

        //Шифр 107
        if (responseEvent is FhssJammingMessage)
        {
            var answer = responseEvent as FhssJammingMessage;
            if (FhssJammingMessageUpdate != null)
                FhssJammingMessageUpdate(answer);
        }

        //Шифр 127
        if (responseEvent is DirectionCorrectionMessage)
        {
            var answer = responseEvent as DirectionCorrectionMessage;
            if (DirectionCorrectionMessageUpdate != null)
                DirectionCorrectionMessageUpdate(answer);
        }

        //Шифр 142
        if (responseEvent is StationLocationMessage)
        {
            var answer = responseEvent as StationLocationMessage;
            if (StationLocationMessageUpdate != null)
                StationLocationMessageUpdate(answer);
        }

        //Шифр 150
        if (responseEvent is SendTextMessage)
        {
            var answer = responseEvent as SendTextMessage;
            if (MasterSlaveSendTextMessageUpdate != null)
                MasterSlaveSendTextMessageUpdate(answer);
        }

        //Шифр 151
        if (responseEvent is SetTimeRequest)
        {
            var answer = responseEvent as SetTimeRequest;
            if (SetTimeUpdate != null)
                SetTimeUpdate(answer);
        }

        //Шифр 152
        if (responseEvent is StorageActionMessage)
        {
            var answer = responseEvent as StorageActionMessage;
            if (StorageActionMessageUpdate != null)
                StorageActionMessageUpdate(answer);
        }

        //Шифр 159
        if (responseEvent is SearchFhssMessage)
        {
            var answer = responseEvent as SearchFhssMessage;
            if (SearchFhssMessageUpdate != null)
                SearchFhssMessageUpdate(answer);
        }

        //Шифр 161
        if (responseEvent is DefaultMessage)
        {
            var answer = responseEvent as DefaultMessage;
            if (GetASPRadioSourcesUpdate != null)
                GetASPRadioSourcesUpdate(answer);
        }

        //Шифр 162
        if (responseEvent is GetRadioSourcesResponse)
        {
            var answer = responseEvent as GetRadioSourcesResponse;
            if (PushASPRadioSourcesUpdate != null)
                PushASPRadioSourcesUpdate(answer);
        }

        //Шифр 163
        if (responseEvent is AntennaDirectionsMessage)
        {
            var answer = responseEvent as AntennaDirectionsMessage;
            if (AntennaDirectionsMessageUpdate != null)
                AntennaDirectionsMessageUpdate(answer);
        }

        //Шифр 166
        if (responseEvent is NumberOfSatellitesUpdateEvent)
        {
            var answer = responseEvent as NumberOfSatellitesUpdateEvent;
            if (NumberOfSatellitesUpdate != null)
                NumberOfSatellitesUpdate(answer);
        }

        //Шифр 170
        if (responseEvent is ShaperConditionUpdateEvent && header.Code == 170)
        {
            var answer = responseEvent as ShaperConditionUpdateEvent;
            if (ShaperModeUpdate != null)
                ShaperModeUpdate(answer);
        }

        //Шифр 171
        if (responseEvent is ShaperVoltageUpdateEvent)
        {
            var answer = responseEvent as ShaperVoltageUpdateEvent;
            if (ShaperVoltageUpdate != null)
                ShaperVoltageUpdate(answer);
        }

        //Шифр 172
        if (responseEvent is ShaperPowerUpdateEvent)
        {
            var answer = responseEvent as ShaperPowerUpdateEvent;
            if (ShaperPowerUpdate != null)
                ShaperPowerUpdate(answer);
        }

        //Шифр 173
        if (responseEvent is ShaperConditionUpdateEvent && header.Code == 173)
        {
            var answer = responseEvent as ShaperConditionUpdateEvent;
            if (ShaperTemperatureUpdate != null)
                ShaperTemperatureUpdate(answer);
        }

        //Шифр 174
        if (responseEvent is ShaperConditionUpdateEvent && header.Code == 174)
        {
            var answer = responseEvent as ShaperConditionUpdateEvent;
            if (ShaperAmperageUpdate != null)
                ShaperAmperageUpdate(answer);
        }

        if (responseEvent == null)
        {
            Console.WriteLine("AWP: responseEvent == null");
        }

    }

    private async Task<TaskCompletionSource<IBinarySerializable>> SendRequest(MessageHeader header, byte[] message)
    {
        using (await asyncLock.LockAsync().ConfigureAwait(false))
        {
            var receiveTask = new TaskCompletionSource<IBinarySerializable>();
            if (!concurrentDictionary.ContainsKey(header.Code))
            {
                concurrentDictionary[header.Code] = new ConcurrentQueue<TaskCompletionSource<IBinarySerializable>>();
            }
            concurrentDictionary[header.Code].Enqueue(receiveTask);

            int count = message.Count();
            try
            {
                await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                if (IsWrite != null)
                    IsWrite(true);
            }
            catch (Exception)
            {
                if (IsWrite != null)
                    IsWrite(false);
                receiveTask.SetException(new Exception("No connection"));
            }

            return receiveTask;
        }
    }

    private async Task SendRequestMini(MessageHeader header, byte[] message)
    {
        using (await asyncLock.LockAsync().ConfigureAwait(false))
        {
            int count = message.Count();
            try
            {
                await client.GetStream().WriteAsync(message, 0, count).ConfigureAwait(false);
                if (IsWrite != null)
                    IsWrite(true);
            }
            catch (Exception)
            {
                if (IsWrite != null)
                    IsWrite(false);
            }
        }
    }


    private static RequestResult GetRequestResult(DefaultMessage response, int expectedCode)
    {
        return response == null || response.Header.Code != expectedCode ? RequestResult.ResponseParseError : (RequestResult)response.Header.ErrorCode;
    }

    private DefaultMessage InvalidParametersInRequest(byte RequestCode)
    {
        MessageHeader header = new MessageHeader(0, 0, RequestCode, 4, 0);
        DefaultMessage answer = new DefaultMessage(header);
        return answer;
    }

    //Шифр 1 
    /*
    public async Task<DefaultMessage> SetSpecialFrequencies(string Type, string MinFrequencyMHz, string MaxFrequencyMHz)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int type = Convert.ToInt32(Type);
            FrequencyType frequencyType = new FrequencyType();
            switch (type)
            {
                case 0:
                    frequencyType = FrequencyType.Forbidden;
                    break;
                case 1:
                    frequencyType = FrequencyType.Known;
                    break;
                case 2:
                    frequencyType = FrequencyType.Important;
                    break;
                default:
                    return InvalidParametersInRequest(1);
            }

            MinFrequencyMHz = MinFrequencyMHz.Replace('.', ',');
            MaxFrequencyMHz = MaxFrequencyMHz.Replace('.', ',');
            double minfreq = Convert.ToDouble(MinFrequencyMHz);
            double maxfreq = Convert.ToDouble(MaxFrequencyMHz);

            int intminfreq = (int)(minfreq * 10000);
            int intmaxfreq = (int)(maxfreq * 10000);

            FrequencyRange frequencyRange = new FrequencyRange(intminfreq, intmaxfreq);

            MessageHeader header = GetMessageHeader(1, 1 + 8);
            byte[] message = SpecialFrequenciesMessage.ToBinary(header, frequencyType, new[] { frequencyRange });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;

            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetSpecialFrequencies(int Type, double MinFrequencyMHz, double MaxFrequencyMHz)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            FrequencyType frequencyType = new FrequencyType();
            switch (Type)
            {
                case 0:
                    frequencyType = FrequencyType.Forbidden;
                    break;
                case 1:
                    frequencyType = FrequencyType.Known;
                    break;
                case 2:
                    frequencyType = FrequencyType.Important;
                    break;
                default:
                    return InvalidParametersInRequest(1);
            }

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            FrequencyRange frequencyRange = new FrequencyRange(intminfreq, intmaxfreq);

            MessageHeader header = GetMessageHeader(1, 1 + 8);
            byte[] message = SpecialFrequenciesMessage.ToBinary(header, frequencyType, new[] { frequencyRange });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;

            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
            var answer = new DefaultMessage(header);
            return answer;
        }

    }
    public async Task<DefaultMessage> SetSpecialFrequencies(FrequencyType frequencyType, FrequencyRange[] frequencies)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 1, length: FrequencyRange.BinarySize * frequencies.Length + 1);
            byte[] message = SpecialFrequenciesMessage.ToBinary(header, frequencyType, frequencies);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    */

    public async Task<DefaultMessage> SetSpecialFrequencies(byte TargetStation, byte frequencyType, FrequencyRange[] frequencies)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(1);
            }

            DspDataModel.FrequencyType freqType = new DspDataModel.FrequencyType();
            switch (frequencyType)
            {
                case 0:
                    freqType = DspDataModel.FrequencyType.Forbidden;
                    break;
                case 1:
                    freqType = DspDataModel.FrequencyType.Known;
                    break;
                case 2:
                    freqType = DspDataModel.FrequencyType.Important;
                    break;
                default:
                    return InvalidParametersInRequest(1);
            }


            MessageHeader header = GetMessageHeader(code: 1, length: 1 + 1 + FrequencyRange.BinarySize * frequencies.Length);
            byte[] message = SpecialFrequenciesMessage.ToBinary(header, freqType, targetStation, frequencies);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetSpecialFrequencies(DspDataModel.TargetStation targetStation, DspDataModel.FrequencyType frequencyType, FrequencyRange[] frequencies)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 1, length: 1 + 1 + FrequencyRange.BinarySize * frequencies.Length);
            byte[] message = SpecialFrequenciesMessage.ToBinary(header, frequencyType, targetStation, frequencies);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 1, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 18
    public async Task<DefaultMessage> SetFilters(string Threshold, string SD, string Bandwidth, string Duration)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int temp = Convert.ToInt32(Threshold);
            if (temp < 0) temp = (-1) * temp;


            SaveThreshold = Convert.ToInt32(Threshold);
            SaveSD = Convert.ToDouble(SD);
            SaveBandwidth = Convert.ToDouble(Bandwidth);
            SaveDuration = Convert.ToInt32(Duration);

            byte threshold = Convert.ToByte(temp);
            short sd = Convert.ToInt16(Convert.ToDouble(SD) * 10);
            int bandwidth = Convert.ToInt32(Convert.ToDouble(Bandwidth) * 10000);
            int duration = Convert.ToInt32(Duration);

            MessageHeader header = GetMessageHeader(18, 11);

            byte[] message = FiltersMessage.ToBinary(header, threshold, sd, bandwidth, duration);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 18, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }

    }
    public async Task<DefaultMessage> SetFilters(int Threshold, double SD, double Bandwidth, int Duration)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            if (Threshold < 0) Threshold = (-1) * Threshold;

            SaveThreshold = Threshold;
            SaveSD = SD;
            SaveBandwidth = Bandwidth;
            SaveDuration = Duration;

            byte threshold = Convert.ToByte(Threshold);
            short sd = Convert.ToInt16(SD * 10);
            int bandwidth = Convert.ToInt32(Bandwidth * 10000);
            int duration = Convert.ToInt32(Duration);

            MessageHeader header = GetMessageHeader(18, 11);

            byte[] message = FiltersMessage.ToBinary(header, threshold, sd, bandwidth, duration);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 18, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFilters(int threshold, float standardDeviation, int bandwidth, TimeSpan duration)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            if (threshold < 0) threshold = (-1) * threshold;

            MessageHeader header = GetMessageHeader(code: 18, length: FiltersMessage.BinarySize - MessageHeader.BinarySize);

            SaveThreshold = Convert.ToInt32(threshold);
            SaveSD = Convert.ToDouble(standardDeviation);
            SaveBandwidth = Convert.ToDouble(bandwidth / 10000.0);
            SaveDuration = Convert.ToInt32((int)duration.TotalMilliseconds);

            byte[] message = FiltersMessage.ToBinary(header, (byte)threshold, (short)(standardDeviation * 10), bandwidth, (int)duration.TotalMilliseconds);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 18, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 2
    public async Task<DefaultMessage> SetMode(int Mode)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspServerMode mode = new DspServerMode();
            switch (Mode)
            {
                case 0:
                    mode = DspServerMode.Stop;
                    break;
                case 1:
                    mode = DspServerMode.RadioIntelligence;
                    break;
                case 2:
                    mode = DspServerMode.RadioIntelligenceWithDf;
                    break;
                case 3:
                    mode = DspServerMode.RadioJammingFrs;
                    break;
                case 4:
                    mode = DspServerMode.RadioJammingAfrs;
                    break;
                case 5:
                    mode = DspServerMode.RadioJammingVoice;
                    break;
                case 6:
                    mode = DspServerMode.RadioJammingFhss;
                    break;
                case 7:
                    mode = DspServerMode.Calibration;
                    break;
                default:
                    return InvalidParametersInRequest(2);
            }

            MessageHeader header = GetMessageHeader(2, 1);
            SaveMode = mode;
            byte[] message = ModeMessage.ToBinary(header, mode);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetMode(DspServerMode mode)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 2, length: ModeMessage.BinarySize - MessageHeader.BinarySize);
            SaveMode = mode;
            byte[] message = ModeMessage.ToBinary(header, mode);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 2, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 3
    public async Task<DefaultMessage> SetSectorsAndRanges(byte RangeType, byte TargetStation, double MinFrequencyMHz, double MaxFrequencyMHz, double MinAngle, double MaxAngle)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.RangeType rangeType;
            switch (RangeType)
            {
                case 0:
                    rangeType = DspDataModel.RangeType.Intelligence;
                    break;
                case 1:
                    rangeType = DspDataModel.RangeType.RadioJamming;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            short intminangle = (short)(MinAngle * 10);
            short intmaxangle = (short)(MaxAngle * 10);

            RangeSector rangeSector = new RangeSector(intminfreq, intmaxfreq, intminangle, intmaxangle);

            MessageHeader header = GetMessageHeader(3, 2 + 12);
            SaveSectorsAndRanges = new[] { rangeSector };
            byte[] message = SectorsAndRangesMessage.ToBinary(header, rangeType, targetStation, new[] { rangeSector });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetSectorsAndRanges(byte RangeType, byte TargetStation, RangeSector rangeSector)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.RangeType rangeType;
            switch (RangeType)
            {
                case 0:
                    rangeType = DspDataModel.RangeType.Intelligence;
                    break;
                case 1:
                    rangeType = DspDataModel.RangeType.RadioJamming;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            MessageHeader header = GetMessageHeader(3, 2 + 12);
            SaveSectorsAndRanges = new[] { rangeSector };
            byte[] message = SectorsAndRangesMessage.ToBinary(header, rangeType, targetStation, new[] { rangeSector });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetSectorsAndRanges(byte RangeType, byte TargetStation, double[] MinFrequencies, double[] MaxFrequencies, double[] MinAngles, double[] MaxAngles)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.RangeType rangeType;
            switch (RangeType)
            {
                case 0:
                    rangeType = DspDataModel.RangeType.Intelligence;
                    break;
                case 1:
                    rangeType = DspDataModel.RangeType.RadioJamming;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }


            int Count = Math.Min(Math.Min(MinFrequencies.Count(), MaxFrequencies.Count()), Math.Min(MinAngles.Count(), MaxAngles.Count()));

            RangeSector[] rangeSectors = new RangeSector[Count];

            for (int i = 0; i < Count; i++)
            {
                int intminfreq = (int)(MinFrequencies[i] * 10000);
                int intmaxfreq = (int)(MaxFrequencies[i] * 10000);

                short intminangle = (short)(MinAngles[i] * 10);
                short intmaxangle = (short)(MaxAngles[i] * 10);

                rangeSectors[i] = new RangeSector(intminfreq, intmaxfreq, intminangle, intmaxangle);
            }

            MessageHeader header = GetMessageHeader(3, 2 + 12 * Count);
            SaveSectorsAndRanges = rangeSectors;
            byte[] message = SectorsAndRangesMessage.ToBinary(header, rangeType, targetStation, rangeSectors);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetSectorsAndRanges(byte RangeType, byte TargetStation, RangeSector[] rangeSectors)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.RangeType rangeType;
            switch (RangeType)
            {
                case 0:
                    rangeType = DspDataModel.RangeType.Intelligence;
                    break;
                case 1:
                    rangeType = DspDataModel.RangeType.RadioJamming;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(3);
            }

            //MessageHeader header = GetMessageHeader(3, 12 * rangeSectors.Count());
            MessageHeader header = GetMessageHeader(3, 2 + RangeSector.BinarySize * rangeSectors.Count());
            //MessageHeader header = GetMessageHeader(code: 3, length: RangeSector.BinarySize * rangeSectors.Length);
            SaveSectorsAndRanges = rangeSectors;
            byte[] message = SectorsAndRangesMessage.ToBinary(header, rangeType, targetStation, rangeSectors);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 3, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 4
    public async Task<DefaultMessage> SetAttenuatorsValue(int BandNumber, int AttenuatorValue, int IsConstAttenuatorEnabled)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(4, 3);

            byte bandNumber = Convert.ToByte(BandNumber);
            byte attenuatorValue = Convert.ToByte(AttenuatorValue);
            byte isConstAttenuatorEnabled = Convert.ToByte(IsConstAttenuatorEnabled);
            AttenuatorSetting attenuatorSetting = new AttenuatorSetting(bandNumber, attenuatorValue, isConstAttenuatorEnabled);

            byte[] message = AttenuatorsMessage.ToBinary(header, new[] { attenuatorSetting });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetAttenuatorsValue(int bandNumber, float attenuatorValue, bool isConstAttenuatorEnabled)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            var setting = new AttenuatorSetting((byte)bandNumber, (byte)(attenuatorValue * 2), (byte)(isConstAttenuatorEnabled ? 1 : 0));
            return await SetAttenuatorsValues(new[] { setting }).ConfigureAwait(false);
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetAttenuatorsValues(AttenuatorSetting[] settings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 4, length: settings.Length * AttenuatorSetting.BinarySize);
            byte[] message = AttenuatorsMessage.ToBinary(header, settings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 4, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 5**

    //Шифр 6
    public async Task<DefaultMessage> SetFRSJamming(byte TargetStation, FRSJammingSetting[] fRSJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(6);
            }

            MessageHeader header = GetMessageHeader(6, 1 + FRSJammingSetting.BinarySize * fRSJammingSettings.Count());
            SaveFRSJammingSettings = fRSJammingSettings;
            byte[] message = FrsJammingMessage.ToBinary(header, targetStation, fRSJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFRSJamming(DspDataModel.TargetStation targetStation, FRSJammingSetting[] fRSJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, 1 + FRSJammingSetting.BinarySize * fRSJammingSettings.Count());
            SaveFRSJammingSettings = fRSJammingSettings;
            byte[] message = FrsJammingMessage.ToBinary(header, targetStation, fRSJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    /*
    public async Task<DefaultMessage> SetFRSJamming(FRSJammingSetting fRSJammingSetting)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize);
            SaveFRSJammingSettings = new[] { fRSJammingSetting };
            byte[] message = FrsJammingMessage.ToBinary(header, new[] { fRSJammingSetting });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFRSJamming(FRSJammingSetting[] fRSJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize * fRSJammingSettings.Count());
            SaveFRSJammingSettings = fRSJammingSettings;
            byte[] message = FrsJammingMessage.ToBinary(header, fRSJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    public async Task<DefaultMessage> SetFRSJamming(int FrequencykHz, byte ModulationCode, byte DeviationCode, byte ManipulationCode, byte DyrationCode, byte Priority, byte Threshold, short Direction, int ID, byte Liter)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize);



            FRSJammingSetting fRSJammingSetting = new FRSJammingSetting(FrequencykHz * 10, ModulationCode, DeviationCode, ManipulationCode, DyrationCode, Priority, Threshold, (short)(Direction * 10), ID, Liter);
            SaveFRSJammingSettings = new[] { fRSJammingSetting };
            byte[] message = FrsJammingMessage.ToBinary(header, new[] { fRSJammingSetting });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFRSJamming(int FrequencykHz, int ModulationCode, int DeviationCode, int ManipulationCode, int DyrationCode, int Priority, int Threshold, int Direction, int ID, int Liter)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize);

            if (Threshold < 0) Threshold = (-1) * Threshold;

            FRSJammingSetting fRSJammingSetting = new FRSJammingSetting(FrequencykHz * 10, (byte)ModulationCode, (byte)DeviationCode, (byte)ManipulationCode, (byte)DyrationCode, (byte)Priority, (byte)Threshold, (short)(Direction * 10), ID, (byte)Liter);
            SaveFRSJammingSettings = new[] { fRSJammingSetting };
            byte[] message = FrsJammingMessage.ToBinary(header, new[] { fRSJammingSetting });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFRSJamming(int[] FrequencykHz, int[] ModulationCode, int[] DeviationCode, int[] ManipulationCode, int[] DyrationCode, int[] Priority, int[] Threshold, int[] Direction, int[] ID, int[] Liter)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize * FrequencykHz.Count());

            for (int i = 0; i < Threshold.Count(); i++)
            {
                if (Threshold[i] < 0) Threshold[i] = (-1) * Threshold[i];
            }
            FRSJammingSetting[] fRSJammingSettings = new FRSJammingSetting[FrequencykHz.Count()];

            for (int i = 0; i < FrequencykHz.Count(); i++)
            {
                fRSJammingSettings[i] = new FRSJammingSetting(FrequencykHz[i] * 10, (byte)ModulationCode[i], (byte)DeviationCode[i], (byte)ManipulationCode[i], (byte)DyrationCode[i], (byte)Priority[i], (byte)Threshold[i], (short)(Direction[i] * 10), ID[i], (byte)Liter[i]);
            }
            SaveFRSJammingSettings = fRSJammingSettings;
            byte[] message = FrsJammingMessage.ToBinary(header, fRSJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    public async Task<DefaultMessage> SetFRSJamming(double FrequencyMHz, int ModulationCode, int DeviationCode, int ManipulationCode, int DyrationCode, int Priority, int Threshold, double Direction, int ID, int Liter)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize);

            if (Threshold < 0) Threshold = (-1) * Threshold;

            int intFrequencykHz = (int)(FrequencyMHz * 10000);
            short shortDirection = (short)(Direction * 10);

            FRSJammingSetting fRSJammingSetting = new FRSJammingSetting(intFrequencykHz, (byte)ModulationCode, (byte)DeviationCode, (byte)ManipulationCode, (byte)DyrationCode, (byte)Priority, (byte)Threshold, shortDirection, ID, (byte)Liter);
            SaveFRSJammingSettings = new[] { fRSJammingSetting };
            byte[] message = FrsJammingMessage.ToBinary(header, new[] { fRSJammingSetting });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFRSJamming(double[] FrequencyMHz, int[] ModulationCode, int[] DeviationCode, int[] ManipulationCode, int[] DyrationCode, int[] Priority, int[] Threshold, double[] Direction, int[] ID, int[] Liter)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(6, FRSJammingSetting.BinarySize * FrequencyMHz.Count());

            for (int i = 0; i < Threshold.Count(); i++)
            {
                if (Threshold[i] < 0) Threshold[i] = (-1) * Threshold[i];
            }

            FRSJammingSetting[] fRSJammingSettings = new FRSJammingSetting[FrequencyMHz.Count()];

            for (int i = 0; i < FrequencyMHz.Count(); i++)
            {
                fRSJammingSettings[i] = new FRSJammingSetting((int)(FrequencyMHz[i] * 10000), (byte)ModulationCode[i], (byte)DeviationCode[i], (byte)ManipulationCode[i], (byte)DyrationCode[i], (byte)Priority[i], (byte)Threshold[i], (short)(Direction[i] * 10), ID[i], (byte)Liter[i]);
            }
            SaveFRSJammingSettings = fRSJammingSettings;
            byte[] message = FrsJammingMessage.ToBinary(header, fRSJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 6, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    */

    //Шифр 7
    /*
    public async Task<DefaultMessage> SetFhssJamming(int Duration, byte FftResolutionCode, FhssJammingSetting[] fhssJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int length = 0;
            for (int i = 0; i < fhssJammingSettings.Count(); i++)
            {
                length += fhssJammingSettings[i].StructureBinarySize;
            }

            if (Duration < 100 || Duration > 5000)
                Duration = 1000;

            DspDataModel.FftResolution FftResolution;
            switch (FftResolutionCode)
            {
                case 2:
                    FftResolution = DspDataModel.FftResolution.N16384;
                    break;
                case 3:
                    FftResolution = DspDataModel.FftResolution.N8192;
                    break;
                case 4:
                    FftResolution = DspDataModel.FftResolution.N4096;
                    break;
                default:
                    FftResolution = DspDataModel.FftResolution.N4096;
                    break;

            }

            MessageHeader header = GetMessageHeader(7, 4 + 1 + 1 + length);
            byte[] message = FhssJammingMessage.ToBinary(header, Duration, FftResolution, (byte)fhssJammingSettings.Length, fhssJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 7, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    */
    public async Task<DefaultMessage> SetFhssJamming(byte TargetStation, int Duration, byte FftResolutionCode, FhssJammingSetting[] fhssJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    return InvalidParametersInRequest(7);
            }

            int length = 0;
            for (int i = 0; i < fhssJammingSettings.Count(); i++)
            {
                length += fhssJammingSettings[i].StructureBinarySize;
            }

            if (Duration < 10 || Duration > 500)
                Duration = 100;

            DspDataModel.FftResolution FftResolution;
            switch (FftResolutionCode)
            {
                case 2:
                    FftResolution = DspDataModel.FftResolution.N16384;
                    break;
                case 3:
                    FftResolution = DspDataModel.FftResolution.N8192;
                    break;
                case 4:
                    FftResolution = DspDataModel.FftResolution.N4096;
                    break;
                default:
                    FftResolution = DspDataModel.FftResolution.N4096;
                    break;

            }

            MessageHeader header = GetMessageHeader(7, 4 + 1 + 1 + 1 + length);
            byte[] message = FhssJammingMessage.ToBinary(header, Duration, FftResolution, targetStation, (byte)fhssJammingSettings.Length, fhssJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            DefaultMessage answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 7, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 8
    public async Task<GetAttenuatorsResponse> GetAttenuatorsValues(int bandNumber)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            var answer = await GetAttenuatorsValues(new[] { bandNumber }).ConfigureAwait(false);
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 8, 1, 0);
            AttenuatorSetting[] attenuatorSettings = new AttenuatorSetting[0];
            GetAttenuatorsResponse answer = new GetAttenuatorsResponse(header, attenuatorSettings);
            return answer;
        }
    }
    public async Task<GetAttenuatorsResponse> GetAttenuatorsValues(int[] bandNumbers)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 8, length: bandNumbers.Length);
            byte[] message = GetAttenuatorsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAttenuatorsResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 8, 1, 0);
            AttenuatorSetting[] attenuatorSettings = new AttenuatorSetting[0];
            GetAttenuatorsResponse answer = new GetAttenuatorsResponse(header, attenuatorSettings);
            return answer;
        }
    }

    //Шифр 9
    public async Task<GetAmplifiersResponse> GetAmplifierValues(int bandNumber)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            var answer = await GetAmplifiersValues(new[] { bandNumber }).ConfigureAwait(false);
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 9, 1, 0);
            byte[] settings = new byte[0];
            GetAmplifiersResponse answer = new GetAmplifiersResponse(header, settings);
            return answer;
        }
    }
    public async Task<GetAmplifiersResponse> GetAmplifiersValues(int[] bandNumbers)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 9, length: bandNumbers.Length);
            byte[] message = GetAttenuatorsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplifiersResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 9, 1, 0);
            byte[] settings = new byte[0];
            GetAmplifiersResponse answer = new GetAmplifiersResponse(header, settings);
            return answer;
        }

    }

    //Шифр 10*

    //Шифр 11
    public async Task<GetRadioSourcesResponse> GetRadioSources()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 11, length: 0);
            byte[] message = GetRadioSourcesRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetRadioSourcesResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 11, 1, 0);
            RadioSource[] radioSources = new RadioSource[0];
            GetRadioSourcesResponse answer = new GetRadioSourcesResponse(header, radioSources);
            return answer;
        }
    }



    //Шифр 12
    public async Task<GetSpectrumResponse> GetSpectrum(string MinFrequencyMHz, string MaxFrequencyMHz, string Coefficient)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MinFrequencyMHz = MinFrequencyMHz.Replace('.', ',');
            MaxFrequencyMHz = MaxFrequencyMHz.Replace('.', ',');
            Coefficient = Coefficient.Replace('.', ',');
            double minfreq = Convert.ToDouble(MinFrequencyMHz);
            double maxfreq = Convert.ToDouble(MaxFrequencyMHz);
            double coefficient = Convert.ToDouble(Coefficient);

            double pointcount = (maxfreq - minfreq) * (GlobalDotsPerMHz / GlobalBandWidthMHz) / coefficient;

            int intpointcount = Convert.ToInt32(pointcount);

            int intminfreq = (int)(minfreq * 10000);
            int intmaxfreq = (int)(maxfreq * 10000);

            MessageHeader header = GetMessageHeader(12, 12);
            byte[] message = GetSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, intpointcount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
            byte firstPointOffSet = 0;
            byte[] spectrum = new byte[0];
            GetSpectrumResponse answer = new GetSpectrumResponse(header, firstPointOffSet, spectrum);
            return answer;
        }
    }
    public async Task<GetSpectrumResponse> GetSpectrum(double MinFrequencyMHz, double MaxFrequencyMHz, double Coefficient)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            double pointcount = (MaxFrequencyMHz - MinFrequencyMHz) * (GlobalDotsPerMHz / GlobalBandWidthMHz) / Coefficient;

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            int intpointcount = Convert.ToInt32(pointcount);

            MessageHeader header = GetMessageHeader(12, 12);
            byte[] message = GetSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, intpointcount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;

            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
            byte firstPointOffSet = 0;
            byte[] spectrum = new byte[0];
            GetSpectrumResponse answer = new GetSpectrumResponse(header, firstPointOffSet, spectrum);
            return answer;
        }
    }
    public async Task<GetSpectrumResponse> GetSpectrum(double MinFrequencyMHz, double MaxFrequencyMHz, int PointCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            MessageHeader header = GetMessageHeader(12, 12);
            byte[] message = GetSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, PointCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            //var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;
            //return answer;

            var answer = await Awaiter<GetSpectrumResponse>(taskCompletionSource.Task, _AwaiterTime);
            answer.Header = CheckCode(12, answer.Header);
            return answer;

        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
            byte firstPointOffSet = 0;
            byte[] spectrum = new byte[0];
            GetSpectrumResponse answer = new GetSpectrumResponse(header, firstPointOffSet, spectrum);
            return answer;
        }
    }
    public async Task<GetSpectrumResponse> GetSpectrumOld(double MinFrequencyMHz, double MaxFrequencyMHz, int PointCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            MessageHeader header = GetMessageHeader(12, 12);
            byte[] message = GetSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, PointCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
            byte firstPointOffSet = 0;
            byte[] spectrum = new byte[0];
            GetSpectrumResponse answer = new GetSpectrumResponse(header, firstPointOffSet, spectrum);
            return answer;
        }
    }
    public async Task<GetSpectrumResponse> GetSpectrum(int startFrequencykHz, int endFrequencykHz, int pointCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 12, length: 12);
            byte[] message = GetSpectrumRequest.ToBinary(header, startFrequencykHz * 10, endFrequencykHz * 10, pointCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 12, 1, 0);
            byte firstPointOffSet = 0;
            byte[] spectrum = new byte[0];
            GetSpectrumResponse answer = new GetSpectrumResponse(header, firstPointOffSet, spectrum);
            return answer;
        }
    }

    //Шифр 13*

    //Шифр 14*

    //Шифр 15 - место вакантно

    //Шифр 16
    public async Task<ExecutiveDFResponse> ExecutiveDF(string MinFrequencyMHz, string MaxFrequencyMHz, string PhaseAveragingCount, string DirectionAveragingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MinFrequencyMHz = MinFrequencyMHz.Replace('.', ',');
            MaxFrequencyMHz = MaxFrequencyMHz.Replace('.', ',');
            double minfreq = Convert.ToDouble(MinFrequencyMHz);
            double maxfreq = Convert.ToDouble(MaxFrequencyMHz);
            byte avphase = Convert.ToByte(PhaseAveragingCount);
            byte avbearing = Convert.ToByte(DirectionAveragingCount);

            int intminfreq = (int)(minfreq * 10000);
            int intmaxfreq = (int)(maxfreq * 10000);


            MessageHeader header = GetMessageHeader(16, 10);


            byte[] message = ExecutiveDFRequest.ToBinary(header, intminfreq, intmaxfreq, avphase, avbearing);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ExecutiveDFResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 16, 1, 0);
            byte[] correlationHistogram = new byte[0];
            ExecutiveDFResponse answer = new ExecutiveDFResponse(header, 0, 0, 0, 0, correlationHistogram);
            return answer;
        }
    }
    public async Task<ExecutiveDFResponse> ExecutiveDF(double MinFrequencyMHz, double MaxFrequencyMHz, int PhaseAveragingCount, int DirectionAveragingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            byte avphase = Convert.ToByte(PhaseAveragingCount);
            byte avbearing = Convert.ToByte(DirectionAveragingCount);

            MessageHeader header = GetMessageHeader(16, 10);

            byte[] message = ExecutiveDFRequest.ToBinary(header, intminfreq, intmaxfreq, avphase, avbearing);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ExecutiveDFResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 16, 1, 0);
            byte[] correlationHistogram = new byte[0];
            ExecutiveDFResponse answer = new ExecutiveDFResponse(header, 0, 0, 0, 0, correlationHistogram);
            return answer;
        }
    }
    public async Task<ExecutiveDFResponse> ExecutiveDfRequest(int startFrequencekHz, int endFrequencykHz, int averagingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 16, length: 10);
            byte[] message = ExecutiveDFRequest.ToBinary(header, startFrequencekHz * 10, endFrequencykHz * 10, (byte)averagingCount, (byte)averagingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ExecutiveDFResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 16, 1, 0);
            byte[] correlationHistogram = new byte[0];
            ExecutiveDFResponse answer = new ExecutiveDFResponse(header, 0, 0, 0, 0, correlationHistogram);
            return answer;
        }

    }

    //Шифр 17
    public async Task<QuasiSimultaneouslyDFResponse> QuasiSimultaneouslyDF(int StartFrequencekHz, int EndFrequencykHz, byte PhaseAveragingCount, byte DirectionAveragingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 17, length: 4 + 4 + 1 + 1);
            byte[] message = QuasiSimultaneouslyDFRequest.ToBinary(header, StartFrequencekHz * 10, EndFrequencykHz * 10, PhaseAveragingCount, DirectionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as QuasiSimultaneouslyDFResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 17, 1, 0);
            RadioSource Sourse = new RadioSource();
            QuasiSimultaneouslyDFResponse answer = new QuasiSimultaneouslyDFResponse(header, Sourse, new [] { new LinkedStationResult() });
            return answer;
        }

    }
    public async Task<QuasiSimultaneouslyDFResponse> QuasiSimultaneouslyDFX10(int StartFrequencekHzX10, int EndFrequencykHzX10, byte PhaseAveragingCount, byte DirectionAveragingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 17, length: 4 + 4 + 1 + 1);
            byte[] message = QuasiSimultaneouslyDFRequest.ToBinary(header, StartFrequencekHzX10, EndFrequencykHzX10, PhaseAveragingCount, DirectionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as QuasiSimultaneouslyDFResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 17, 1, 0);
            RadioSource Sourse = new RadioSource();
            QuasiSimultaneouslyDFResponse answer = new QuasiSimultaneouslyDFResponse(header, Sourse, new[] { new LinkedStationResult() });
            return answer;
        }

    }

    //Шифр 19*
    public async Task<TechAppSpectrumResponse> GetTechAppSpectrum(int bandNumber, int averagingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 19, length: TechAppSpectrumRequest.BinarySize - MessageHeader.BinarySize);
            byte[] message = TechAppSpectrumRequest.ToBinary(header, (byte)bandNumber, (byte)averagingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as TechAppSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 19, 1, 0);
            Scan[] scans = new Scan[0];
            TechAppSpectrumResponse answer = new TechAppSpectrumResponse(header, scans);
            return answer;
        }
    }

    //Шифр 20*
    public async Task<DefaultMessage> SetReceiversChannel(byte receiversChannel)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            ReceiverChannel Channel = new ReceiverChannel();
            switch (receiversChannel)
            {
                case 0:
                    Channel = ReceiverChannel.Air;
                    break;
                case 1:
                    Channel = ReceiverChannel.ExternalCalibration;
                    break;
                case 2:
                    Channel = ReceiverChannel.NoiseGenerator;
                    break;
                default:
                    Channel = ReceiverChannel.Air;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 20, length: 1);
            byte[] message = SetReceiversChannelRequest.ToBinary(header, Channel);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            //var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            //return answer;

            var answer = await Awaiter<DefaultMessage>(taskCompletionSource.Task, 500);
            answer.Header = CheckCode(20, answer.Header);
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 20, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    private MessageHeader CheckCode(byte Code, MessageHeader answerHeader)
    {
        if (answerHeader.Code == 0)
        {
            return new MessageHeader(0, 0, Code, 5, 0);
        }
        else return answerHeader;
    }

    private async Task<T> Ws<T>(int timeout) where T : class, IBinarySerializable, new()
    {
        await Task.Delay(timeout);
        T temp = new T();
        return temp;
    }

    private async Task<T> Awaiter<T>(Task<IBinarySerializable> task, int timeout) where T : class, IBinarySerializable, new()
    {
        var task2 = Ws<T>(timeout);

        var answer = new T();

        if (await Task.WhenAny(task, task2) == task)
        {
            answer = task.Result as T;
            //Console.WriteLine("task completed within timeout");
        }
        else
        {
            answer = task2.Result as T;
            //Console.WriteLine("timeout logic");
        }
        return answer;
    }

    //Шифр 21
    public async Task<HeterodyneRadioSourcesResponse> HeterodyneRadioSources(double StartFrequencyMHz, double EndFrequencyMHz, double StepMHz, int PhaseAveragingCount, int DirectionAveragingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(21, 11);

            int startFrequency = (int)(StartFrequencyMHz * 10000);
            int endFrequency = (int)(EndFrequencyMHz * 10000);

            int iStepMHz = (int)(StepMHz * 10);
            if (iStepMHz < 0 || iStepMHz > 255)
                iStepMHz = 50;

            byte[] message = HeterodyneRadioSourcesRequest.ToBinary(header, startFrequency, endFrequency, (byte)iStepMHz, (byte)PhaseAveragingCount, (byte)DirectionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as HeterodyneRadioSourcesResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 21, 1, 0);
            HeterodyneRadioSouce[] heterodyneRadioSouces = new HeterodyneRadioSouce[0];
            HeterodyneRadioSourcesResponse answer = new HeterodyneRadioSourcesResponse(header, heterodyneRadioSouces);
            return answer;
        }

    }
    public async Task<HeterodyneRadioSourcesResponse> HeterodyneRadioSources(int startFrequencekHz, int endFrequencykHz, int StepkHz, int PhaseAveragingCount, int DirectionAveragingCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 21, length: 11);

            int iStepkHz = (int)(StepkHz / 100);
            if (iStepkHz < 0 || iStepkHz > 255)
                iStepkHz = 50;

            byte[] message = HeterodyneRadioSourcesRequest.ToBinary(header, startFrequencekHz * 10, endFrequencykHz * 10, (byte)iStepkHz, (byte)PhaseAveragingCount, (byte)DirectionAveragingCount);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as HeterodyneRadioSourcesResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 21, 1, 0);
            HeterodyneRadioSouce[] heterodyneRadioSouces = new HeterodyneRadioSouce[0];
            HeterodyneRadioSourcesResponse answer = new HeterodyneRadioSourcesResponse(header, heterodyneRadioSouces);
            return answer;
        }
    }

    //Шифр 22
    public async Task<GetCalibrationProgressResponse> GetCalibrationProgress()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 22, length: 0);
            byte[] message = GetCalibrationProgressRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetCalibrationProgressResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 22, 1, 0);
            byte[] BPD = new byte[0];
            GetCalibrationProgressResponse answer = new GetCalibrationProgressResponse(header, 0, BPD);
            return answer;
        }
    }

    //Шифр 23
    public async Task<GetBandAmplitudeLevelsResponse> GetBandAmplitudeLevels(int BandNumber)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(23, 1);

            byte bandNumber = Convert.ToByte(BandNumber);

            byte[] message = GetBandAmplitudeLevelsRequest.ToBinary(header, new[] { bandNumber });

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBandAmplitudeLevelsResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 23, 1, 0);
            BandAmplitudeLevel[] BALs = new BandAmplitudeLevel[0];
            GetBandAmplitudeLevelsResponse answer = new GetBandAmplitudeLevelsResponse(header, BALs);
            return answer;
        }
    }
    public async Task<GetBandAmplitudeLevelsResponse> GetBandAmplitudeLevels(int[] BandNumbers)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(23, BandNumbers.Count());

            byte[] bandNumbers = BandNumbers.Select(x => Convert.ToByte(x)).ToArray();

            byte[] message = GetBandAmplitudeLevelsRequest.ToBinary(header, bandNumbers);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBandAmplitudeLevelsResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 23, 1, 0);
            BandAmplitudeLevel[] BALs = new BandAmplitudeLevel[0];
            GetBandAmplitudeLevelsResponse answer = new GetBandAmplitudeLevelsResponse(header, BALs);
            return answer;
        }
    }
    public async Task<GetBandAmplitudeLevelsResponse> GetBandLevels(int[] bandNumbers)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 23, length: bandNumbers.Length);
            byte[] message = GetBandAmplitudeLevelsRequest.ToBinary(header, bandNumbers.Select(n => (byte)n).ToArray());

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBandAmplitudeLevelsResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 23, 1, 0);
            BandAmplitudeLevel[] BALs = new BandAmplitudeLevel[0];
            GetBandAmplitudeLevelsResponse answer = new GetBandAmplitudeLevelsResponse(header, BALs);
            return answer;
        }
    }

    //Шифр 24
    public async Task<GetAmplitudeTimeSpectrumResponse> GetAmplitudeTimeSpectrum(double MinFrequencyMHz, double MaxFrequencyMHz, double Coefficient)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            NetworkStream netStream = client.GetStream();

            double pointcount = (MaxFrequencyMHz - MinFrequencyMHz) * (GlobalDotsPerMHz / GlobalBandWidthMHz) / Coefficient;

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            int intpointcount = Convert.ToInt32(pointcount);

            MessageHeader header = GetMessageHeader(24, 13);
            byte[] message = GetAmplitudeTimeSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, intpointcount, 60);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 24, 1, 0);
            byte[] spectrum = new byte[0];
            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse(header, spectrum);
            return answer;
        }
    }
    public async Task<GetAmplitudeTimeSpectrumResponse> GetAmplitudeTimeSpectrum(double MinFrequencyMHz, double MaxFrequencyMHz, double Coefficient, byte TimeLength)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            NetworkStream netStream = client.GetStream();

            double pointcount = (MaxFrequencyMHz - MinFrequencyMHz) * (GlobalDotsPerMHz / GlobalBandWidthMHz) / Coefficient;

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            int intpointcount = Convert.ToInt32(pointcount);

            MessageHeader header = GetMessageHeader(24, 13);
            byte[] message = GetAmplitudeTimeSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, intpointcount, TimeLength);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 24, 1, 0);
            byte[] spectrum = new byte[0];
            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse(header, spectrum);
            return answer;
        }
    }

    public async Task<GetAmplitudeTimeSpectrumResponse> GetAmplitudeTimeSpectrum(double MinFrequencyMHz, double MaxFrequencyMHz, int PointCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            NetworkStream netStream = client.GetStream();

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            MessageHeader header = GetMessageHeader(24, 13);
            byte[] message = GetAmplitudeTimeSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, PointCount, 60);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 24, 1, 0);
            byte[] spectrum = new byte[0];
            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse(header, spectrum);
            return answer;
        }
    }
    public async Task<GetAmplitudeTimeSpectrumResponse> GetAmplitudeTimeSpectrum(double MinFrequencyMHz, double MaxFrequencyMHz, int PointCount, byte TimeLength)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            NetworkStream netStream = client.GetStream();

            int intminfreq = (int)(MinFrequencyMHz * 10000);
            int intmaxfreq = (int)(MaxFrequencyMHz * 10000);

            MessageHeader header = GetMessageHeader(24, 13);
            byte[] message = GetAmplitudeTimeSpectrumRequest.ToBinary(header, intminfreq, intmaxfreq, PointCount, TimeLength);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 24, 1, 0);
            byte[] spectrum = new byte[0];
            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse(header, spectrum);
            return answer;
        }
    }

    public async Task<GetAmplitudeTimeSpectrumResponse> GetAmplitudeTimeSpectrum(int startFrequencykHz, int endFrequencykHz, int pointCount)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 24, length: 13);
            byte[] message = GetAmplitudeTimeSpectrumRequest.ToBinary(header, startFrequencykHz * 10, endFrequencykHz * 10, pointCount, 60);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 24, 1, 0);
            byte[] spectrum = new byte[0];
            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse(header, spectrum);
            return answer;
        }
    }
    public async Task<GetAmplitudeTimeSpectrumResponse> GetAmplitudeTimeSpectrum(int startFrequencykHz, int endFrequencykHz, int pointCount, byte TimeLength)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 24, length: 13);
            byte[] message = GetAmplitudeTimeSpectrumRequest.ToBinary(header, startFrequencykHz * 10, endFrequencykHz * 10, pointCount, TimeLength);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAmplitudeTimeSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 24, 1, 0);
            byte[] spectrum = new byte[0];
            GetAmplitudeTimeSpectrumResponse answer = new GetAmplitudeTimeSpectrumResponse(header, spectrum);
            return answer;
        }
    }

    //Шифр 25
    public async Task<RequestResult> StartRadioRecording()
    {
        if (client == null)
            return RequestResult.NoConnection;
        if (!client.Connected)
        {
            return RequestResult.NoConnection;
        }
        const int code = 25;
        MessageHeader header = GetMessageHeader(code, length: 0);
        byte[] message = StartRecordingRequest.ToBinary(header);

        var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
        var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
        return GetRequestResult(answer, code);
    }

    //Шифр 26
    public async Task<int> StopRadioRecording()
    {
        if (client == null)
            return -1;
        if (!client.Connected)
        {
            return -1;
        }
        const int code = 26;
        MessageHeader header = GetMessageHeader(code, length: 0);
        byte[] message = StopRecordingRequest.ToBinary(header);

        var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
        var answer = await taskCompletionSource.Task.ConfigureAwait(false) as StopRecordingResponse;
        return answer.RecordId;

    }

    //Шифр 27
    public async Task<DefaultMessage> SetDirectionCorrection(double headingAngle, bool UseCorrection)
    {

        if (client == null)
            return null;
        if (client.Connected)
        {
            short shortheadingAngle = Convert.ToInt16(headingAngle * 10);

            MessageHeader header = GetMessageHeader(27, 2 + 1);

            byte[] message = DirectionCorrectionMessage.ToBinary(header, shortheadingAngle, UseCorrection);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 27, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 28**

    //Шифр 29 - событие, см. выше

    //Шифр 30
    public async Task<DefaultMessage> CalculateCalibrationCorrection(CalibrationCorrectionSignal[] Signals)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(30, CalibrationCorrectionSignal.BinarySize * Signals.Count());

            byte[] message = CalculateCalibrationCorrectionRequest.ToBinary(header, Signals);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 30, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 31
    public async Task<GetAdaptiveThresholdResponse> GetAdaptiveThreshold(int BandNumber)
    {

        if (client == null)
            return null;
        if (client.Connected)
        {
            byte bandNumber = Convert.ToByte(BandNumber);

            MessageHeader header = GetMessageHeader(31, 1);

            byte[] message = GetAdaptiveThresholdRequest.ToBinary(header, bandNumber);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAdaptiveThresholdResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 31, 1, 0);
            GetAdaptiveThresholdResponse answer = new GetAdaptiveThresholdResponse(header, 0);
            return answer;
        }
    }
    public async Task<GetAdaptiveThresholdResponse> GetAdaptiveThreshold(byte BandNumber)
    {

        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(31, 1);

            byte[] message = GetAdaptiveThresholdRequest.ToBinary(header, BandNumber);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetAdaptiveThresholdResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 31, 1, 0);
            GetAdaptiveThresholdResponse answer = new GetAdaptiveThresholdResponse(header, 0);
            return answer;
        }
    }

    //Шифр 32
    public async Task<DefaultMessage> SetFrsRadioJamSettings(int EmitionDuration, int LongWorkingSignalDuration, byte ChannelsInLiter)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(32, SetFrsRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);

            byte[] message = SetFrsRadioJamSettingsRequest.ToBinary(header, EmitionDuration, LongWorkingSignalDuration, ChannelsInLiter);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 32, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFrsRadioJamSettings(int EmitionDuration, int LongWorkingSignalDuration, int ChannelsInLiter)
    {

        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(32, SetFrsRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);

            byte[] message = SetFrsRadioJamSettingsRequest.ToBinary(header, EmitionDuration, LongWorkingSignalDuration, (byte)ChannelsInLiter);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 32, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 33 - событие, см. выше

    //Шифр 34
    public async Task<GetFhssNetworksResponse> GetFhssNetworks()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 34, length: 0);
            byte[] message = GetFhssNetworksRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetFhssNetworksResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 34, 1, 0);
            FhssRadioNetwork[] fhssRadioNetwork = new FhssRadioNetwork[0];
            GetFhssNetworksResponse answer = new GetFhssNetworksResponse(header, 0, fhssRadioNetwork);
            return answer;
        }
    }

    //Шифр 35
    /*
    public async Task<SpecialFrequenciesMessage> GetSpecialFrequencies(FrequencyType frequencyType)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {

            MessageHeader header = GetMessageHeader(code: 35, length: 1);
            byte[] message = GetSpecialFrequenciesRequest.ToBinary(header, frequencyType);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SpecialFrequenciesMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 35, 1, 0);
            FrequencyRange[] frequencyRange = new FrequencyRange[0];
            SpecialFrequenciesMessage answer = new SpecialFrequenciesMessage(header, 0, frequencyRange);
            return answer;
        }
    }
    */
    public async Task<SpecialFrequenciesMessage> GetSpecialFrequencies(byte TargetStation, DspDataModel.FrequencyType frequencyType)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 35, length: 2);
            byte[] message = GetSpecialFrequenciesRequest.ToBinary(header, frequencyType, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SpecialFrequenciesMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 35, 1, 0);
            FrequencyRange[] frequencyRange = new FrequencyRange[0];
            SpecialFrequenciesMessage answer = new SpecialFrequenciesMessage(header, 0, 0, frequencyRange);
            return answer;
        }
    }
    public async Task<SpecialFrequenciesMessage> GetSpecialFrequencies(DspDataModel.TargetStation targetStation, DspDataModel.FrequencyType frequencyType)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {

            MessageHeader header = GetMessageHeader(code: 35, length: 2);
            byte[] message = GetSpecialFrequenciesRequest.ToBinary(header, frequencyType, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SpecialFrequenciesMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 35, 1, 0);
            FrequencyRange[] frequencyRange = new FrequencyRange[0];
            SpecialFrequenciesMessage answer = new SpecialFrequenciesMessage(header, 0, 0, frequencyRange);
            return answer;
        }
    }

    //Шифр 36
    public async Task<FiltersMessage> GetFilters()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 36, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FiltersMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 36, 1, 0);
            FiltersMessage answer = new FiltersMessage(header, 0, 0, 0, 0);
            return answer;
        }
    }

    //Шифр 37
    public async Task<ModeMessage> GetMode()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 37, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as ModeMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 37, 1, 0);
            ModeMessage answer = new ModeMessage(header, 0);
            return answer;
        }
    }

    //Шифр 38
    public async Task<SectorsAndRangesMessage> GetSectorsAndRanges(byte RangeType, byte TargetStation)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.RangeType rangeType;
            switch (RangeType)
            {
                case 0:
                    rangeType = DspDataModel.RangeType.Intelligence;
                    break;
                case 1:
                    rangeType = DspDataModel.RangeType.RadioJamming;
                    break;
                default:
                    rangeType = DspDataModel.RangeType.Intelligence;
                    break;
            }

            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 38, length: 2);
            byte[] message = GetSectorsAndRangesRequest.ToBinary(header, rangeType, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SectorsAndRangesMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 38, 1, 0);
            RangeSector[] rangeSectors = new RangeSector[0];
            SectorsAndRangesMessage answer = new SectorsAndRangesMessage(header, DspDataModel.RangeType.RadioJamming, DspDataModel.TargetStation.Current, rangeSectors);
            return answer;
        }
    }

    //Шифр 39
    /*
    public async Task<FrsJammingMessage> GetFRSJamming()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 39, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FrsJammingMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 39, 1, 0);
            FRSJammingSetting[] FRSJammingSettings = new FRSJammingSetting[0];
            FrsJammingMessage answer = new FrsJammingMessage(header, FRSJammingSettings);
            return answer;
        }
    }
    */
    public async Task<FrsJammingMessage> GetFRSJamming(byte TargetStation)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 39, length: 1);
            byte[] message = GetFrsJammingRequest.ToBinary(header, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FrsJammingMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 39, 1, 0);
            FRSJammingSetting[] FRSJammingSettings = new FRSJammingSetting[0];
            FrsJammingMessage answer = new FrsJammingMessage(header, 0, FRSJammingSettings);
            return answer;
        }
    }

    //Шифр 40
    /*
    public async Task<FhssJammingMessage> GetFhssJamming()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 40, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FhssJammingMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 40, 1, 0);
            FhssJammingSetting[] fhssJammingSettings = new FhssJammingSetting[1];
            fhssJammingSettings[0].FixedRadioSources = new FhssFixedRadioSource[0];
            FhssJammingMessage answer = new FhssJammingMessage(header, 0, 0, 0, fhssJammingSettings);
            return answer;
        }
    }
    */
    public async Task<FhssJammingMessage> GetFhssJamming(byte TargetStation)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 40, length: 1);
            byte[] message = GetFhssJammingRequest.ToBinary(header, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as FhssJammingMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 40, 1, 0);
            FhssJammingSetting[] fhssJammingSettings = new FhssJammingSetting[1];
            fhssJammingSettings[0].FixedRadioSources = new FhssFixedRadioSource[0];
            FhssJammingMessage answer = new FhssJammingMessage(header, 0, 0, 0, 0, fhssJammingSettings);
            return answer;
        }
    }

    //Шифр 41
    public async Task<GetScanSpeedResponse> GetScanSpeed()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 41, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetScanSpeedResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 41, 1, 0);
            GetScanSpeedResponse answer = new GetScanSpeedResponse(header, 0, 0);
            return answer;
        }
    }

    //Шифр 42
    /*
    public async Task<DefaultMessage> SetStationLocation(double Latitude, double Longitude, short Altitude, bool UseLocation)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 42, length: 8 + 8 + 2 + 1);
            byte[] message = StationLocationMessage.ToBinary(header, Latitude, Longitude, Altitude, UseLocation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 42, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    */
    public async Task<DefaultMessage> SetStationLocation(byte TargetStation, double Latitude, double Longitude, short Altitude, bool UseLocation)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 42, length: 8 + 8 + 2 + 1 + 1);
            byte[] message = StationLocationMessage.ToBinary(header, Latitude, Longitude, Altitude, UseLocation, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 42, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 43
    /*
    public async Task<StationLocationMessage> GetStationLocation()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 43, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as StationLocationMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 43, 1, 0);
            StationLocationMessage answer = new StationLocationMessage(header, -1, -1, -1111, false);
            return answer;
        }
    }
    */
    public async Task<StationLocationMessage> GetStationLocation(byte TargetStation)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.TargetStation targetStation;
            switch (TargetStation)
            {
                case 0:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
                case 1:
                    targetStation = DspDataModel.TargetStation.Linked;
                    break;
                default:
                    targetStation = DspDataModel.TargetStation.Current;
                    break;
            }

            MessageHeader header = GetMessageHeader(code: 43, length: 1);
            byte[] message = GetStationLocationRequest.ToBinary(header, targetStation);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as StationLocationMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 43, 1, 0);
            StationLocationMessage answer = new StationLocationMessage(header, -1, -1, -1111, false, 0);
            return answer;
        }
    }

    //Шифр 44
    public async Task<GetRadioControlSpectrumResponse> GetRadioControlSpectrum(byte BandNumber)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 44, length: 1);
            byte[] message = GetRadioControlSpectrumRequest.ToBinary(header, BandNumber);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetRadioControlSpectrumResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 44, 1, 0);
            byte[] Spectrum = new byte[0];
            GetRadioControlSpectrumResponse answer = new GetRadioControlSpectrumResponse(header, Spectrum);
            return answer;
        }
    }

    //Шифр 45 - событие, см. выше

    //Шифр 46 - событие, см. выше

    //Шифр 47
    public async Task<DefaultMessage> SetStationRole(byte stationRole, byte OwnAddress, byte LinkedAddress)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            StationRole Role = new StationRole();
            switch (stationRole)
            {
                case 0:
                    Role = StationRole.Standalone;
                    break;
                case 1:
                    Role = StationRole.Master;
                    break;
                case 2:
                    Role = StationRole.Slave;
                    break;
                default:
                    return InvalidParametersInRequest(47);
            }

            MessageHeader header = GetMessageHeader(code: 47, length: 3);
            byte[] message = SetStationRoleRequest.ToBinary(header, Role, OwnAddress, LinkedAddress);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;

            if (answer.Header.ErrorCode == 0)
                OwnServerAddress = OwnAddress;

            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 47, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 48
    public async Task<DefaultMessage> InitMasterSlave(string Address)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 48, length: 4 + 2 * Address.Length);
            byte[] message = InitMasterSlaveRequest.ToBinary(header, Address);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 48, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 49
    public async Task<DefaultMessage> DisconnectFromMasterSlaveStation()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 49, length: 0);
            byte[] message = DisconnectFromMasterSlaveStationRequest.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 49, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 50
    public async Task<DefaultMessage> SendTextMasterSlaveMessage(string Text)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 50, length: 4 + 2 * Text.Length);
            byte[] message = SendTextMessage.ToBinary(header, Text);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 50, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 51
    public async Task<DefaultMessage> SetMasterSlaveTime(byte Hour, byte Minute, byte Second, short Millisecond)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 51, length: 1 + 1 + 1 + 2);
            byte[] message = SetTimeRequest.ToBinary(header, Hour, Minute, Second, Millisecond);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 51, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 52
    public async Task<DefaultMessage> StorageAction(byte storageType, byte signalAction, int[] SignalsId)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            DspDataModel.StorageType Storage = new DspDataModel.StorageType();
            switch (storageType)
            {
                case 0:
                    Storage = DspDataModel.StorageType.Frs;
                    break;
                case 1:
                    Storage = DspDataModel.StorageType.Fhss;
                    break;
                default:
                    return InvalidParametersInRequest(52);
            }

            DspDataModel.SignalAction Action = new DspDataModel.SignalAction();
            switch (signalAction)
            {
                case 0:
                    Action = DspDataModel.SignalAction.Hide;
                    break;
                case 1:
                    Action = DspDataModel.SignalAction.Restore;
                    break;
                default:
                    return InvalidParametersInRequest(52);
            }

            MessageHeader header = GetMessageHeader(code: 52, length: 1 + 1 + SignalsId.Length * 4);
            byte[] message = StorageActionMessage.ToBinary(header, Storage, Action, SignalsId);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 52, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 53
    public async Task<GetBearingPanoramaSignalsResponse> GetBearingPanoramaSignals(double StartFrequencyMHz, double EndFrequencyMHz)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int StartFrequencykHz = (int)(StartFrequencyMHz * 10000.0);
            int EndFrequencykHz = (int)(EndFrequencyMHz * 10000.0);

            MessageHeader header = GetMessageHeader(code: 53, length: 4 + 4);
            byte[] message = GetBearingPanoramaSignalsRequest.ToBinary(header, StartFrequencykHz, EndFrequencykHz);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBearingPanoramaSignalsResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 53, 1, 0);
            int ImpulseSignalsCount = 0;
            BearingPanoramaSignal[] ImpulseSignals = new BearingPanoramaSignal[0];
            int FixedSignalsCount = 0;
            BearingPanoramaSignal[] FixedSignals = new BearingPanoramaSignal[0];
            GetBearingPanoramaSignalsResponse answer = new GetBearingPanoramaSignalsResponse(header, ImpulseSignalsCount, ImpulseSignals, FixedSignalsCount, FixedSignals);
            return answer;
        }
    }

    //Шифр 54
    public async Task<DefaultMessage> SetFrsAutoRadioJamSettings(int EmitionDurationms, int LongWorkingSignalDurationms, byte ChannelsInLiter, int SignalBandwidthThresholdkHzX10, byte Threshold)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(54, SetFrsAutoRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);

            byte[] message = SetFrsAutoRadioJamSettingsRequest.ToBinary(header, EmitionDurationms, LongWorkingSignalDurationms, ChannelsInLiter, SignalBandwidthThresholdkHzX10, Threshold);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 54, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetFrsAutoRadioJamSettings(int EmitionDurationms, int LongWorkingSignalDurationms, byte ChannelsInLiter, int SignalBandwidthThresholdKHzX10, int Threshold)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(54, SetFrsAutoRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);

            if (Threshold < 0) Threshold = (-1) * Threshold;

            byte[] message = SetFrsAutoRadioJamSettingsRequest.ToBinary(header, EmitionDurationms, LongWorkingSignalDurationms, ChannelsInLiter, SignalBandwidthThresholdKHzX10, (byte)Threshold);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 54, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 55
    public async Task<DefaultMessage> SetAfrsRadioJamSettings(int EmitionDurationms, int LongWorkingSignalDurationms, byte ChannelsInLiter, int SearchBandwidthKHzX10, short DirectionSearchSectorX10, byte Threshold)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(55, SetAfrsRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);

            byte[] message = SetAfrsRadioJamSettingsRequest.ToBinary(header, EmitionDurationms, LongWorkingSignalDurationms, ChannelsInLiter, SearchBandwidthKHzX10, DirectionSearchSectorX10, Threshold);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 55, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    public async Task<DefaultMessage> SetAfrsRadioJamSettings(int EmitionDurationms, int LongWorkingSignalDurationms, byte ChannelsInLiter, int SearchBandwidthKHzX10, short DirectionSearchSectorX10, int Threshold)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(55, SetAfrsRadioJamSettingsRequest.BinarySize - MessageHeader.BinarySize);

            if (Threshold < 0) Threshold = (-1) * Threshold;

            byte[] message = SetAfrsRadioJamSettingsRequest.ToBinary(header, EmitionDurationms, LongWorkingSignalDurationms, ChannelsInLiter, SearchBandwidthKHzX10, DirectionSearchSectorX10, (byte)Threshold);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 55, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 56 - событие, см. выше

    //Шифр 57
    public async Task<DirectionCorrectionMessage> GetDirectionCorrection()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 57, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DirectionCorrectionMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 57, 1, 3);
            DirectionCorrectionMessage answer = new DirectionCorrectionMessage(header, -1, false);
            return answer;
        }
    }

    //Шифр 58
    public async Task<SearchFhssMessage> GetSearchFHSS()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 58, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as SearchFhssMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 58, 1, 1);
            SearchFhssMessage answer = new SearchFhssMessage(header, 255);
            return answer;
        }
    }

    //Шифр 59
    public async Task<DefaultMessage> SetSearchFHSS(byte SearchFhss)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 59, length: 1);
            byte[] message = SearchFhssMessage.ToBinary(header, SearchFhss);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 59, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 60
    public async Task<MasterSlaveStateChangedEvent> MasterSlaveState(byte Role)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 60, length: 2);
            StationRole stationRole;
            switch (Role)
            {
                case 0:
                    stationRole = StationRole.Standalone;
                    break;
                case 1:
                    stationRole = StationRole.Master;
                    break;
                case 2:
                    stationRole = StationRole.Slave;
                    break;
                default:
                    stationRole = StationRole.Standalone;
                    break;

            }
            byte[] message = MasterSlaveStateChangedEvent.ToBinary(header, stationRole, ConnectionState.NoConnection);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as MasterSlaveStateChangedEvent;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 60, 1, 0);
            MasterSlaveStateChangedEvent answer = new MasterSlaveStateChangedEvent(header, StationRole.Standalone, ConnectionState.NoConnection);
            return answer;
        }
    }
    public async Task<MasterSlaveStateChangedEvent> MasterSlaveState(StationRole Role)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 60, length: 2);
            byte[] message = MasterSlaveStateChangedEvent.ToBinary(header, Role, ConnectionState.NoConnection);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as MasterSlaveStateChangedEvent;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 60, 1, 0);
            MasterSlaveStateChangedEvent answer = new MasterSlaveStateChangedEvent(header, StationRole.Standalone, ConnectionState.NoConnection);
            return answer;
        }
    }

    //Шифр 61
    public async Task<DefaultMessage> GetASPRadioSources()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 61, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 61, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }

    }

    //Шифр 62
    public async Task<DefaultMessage> PushASPRadioSources(RadioSource[] RadioSources)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 62, length: RadioSource.BinarySize * RadioSources.Length);
            byte[] message = GetRadioSourcesResponse.ToBinary(header, RadioSources);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 62, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шфир 63
    public async Task<DefaultMessage> SetAntennaDirections(AntennaDirections antennaDirections)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 63, length: AntennaDirections.BinarySize);
            byte[] message = AntennaDirectionsMessage.ToBinary(header, antennaDirections);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 63, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }
    public async Task<DefaultMessage> SetAntennaDirections(short Ard1, short Ard2, short Ard3, short Compass, short Lpa1_3, short Lpa2_4)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 63, length: AntennaDirections.BinarySize);
            AntennaDirections antennaDirections = new AntennaDirections(Ard1, Ard2, Ard3, Compass, Lpa1_3, Lpa2_4);
            byte[] message = AntennaDirectionsMessage.ToBinary(header, antennaDirections);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 63, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 64 - см. выше

    //Шифр 65
    public async Task<DefaultMessage> InquireFHHS(int ID)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 65, length: 2);
            byte[] message = GetTemperatureResponse.ToBinary(header, (short)ID);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 65, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 66
    public async Task<DefaultMessage> ResetGNSS()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 66, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 66, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 67
    public async Task<DefaultMessage> SetGpsSyncInterval(int intSync)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 67, length: 4);
            byte[] message = SetGpsSyncIntervalRequest.ToBinary(header, intSync);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 67, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 68
    public async Task<DefaultMessage> SetGpsPositionError(short ErrorCoord)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 68, length: 2);
            byte[] message = SetGpsPositionErrorRequest.ToBinary(header, ErrorCoord);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 68, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 69 - временный проверочный
    public async void Test69()
    {
        if (client != null)
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 69, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);
            await SendRequestMini(header, message);
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 69, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
        }
    }

    //Шифр 70
    public async Task<DefaultMessage> FHSGetState()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 70, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 70, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 71
    public async Task<DefaultMessage> FHSGetParam()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 71, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 71, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 72
    public async Task<DefaultMessage> FHSGetParam(byte SwitchAntenna)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 72, length: 1);
            byte[] message = SetShaperAntennaRequest.ToBinary(header, SwitchAntenna);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 72, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 73
    public async Task<DefaultMessage> SwitchOffRadiat()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 73, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 73, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 74
    public async Task<DefaultMessage> SwitchOnFWS(FRSJammingSetting[] fRSJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 74, length: FRSJammingSetting.BinarySize * fRSJammingSettings.Count());
            byte[] message = ShaperStartFrsJammingRequest.ToBinary(header, fRSJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 74, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 75
    public async Task<DefaultMessage> SwitchOnFHSS(FhssJammingSetting[] fhssJammingSettings)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            int length = 0;
            for (int i = 0; i < fhssJammingSettings.Count(); i++)
            {
                length += fhssJammingSettings[i].StructureBinarySize;
            }

            MessageHeader header = GetMessageHeader(code: 75, length: 2 + length);
            byte[] message = Protocols.ShaperStartFhssJammingRequest.ToBinary(header, (short)fhssJammingSettings.Count(), fhssJammingSettings);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 75, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 76
    public async Task<DefaultMessage> ResetFHS()
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 76, length: 0);
            byte[] message = DefaultMessage.ToBinary(header);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 76, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 77
    public async Task<DefaultMessage> SetShaperStateUpdateInterval(short ShaperStateUpdateInterval)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 77, length: 2);
            byte[] message = SetShaperStateUpdateIntervalRequest.ToBinary(header, ShaperStateUpdateInterval);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 77, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 78
    public async Task<DefaultMessage> SetShaperSettingsUpdateInterval(short ShaperSettingsUpdateInterval)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 78, length: 2);
            byte[] message = SetShaperSettingsUpdateIntervalRequest.ToBinary(header, ShaperSettingsUpdateInterval);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 78, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 79
    public async Task<GetBandsAdaptiveThresholdResponse> GetBandsAdaptiveThreshold(byte [] Bands)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 79, length: Bands.Count());
            byte[] message = GetBandsAdaptiveThresholdRequest.ToBinary(header, Bands);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as GetBandsAdaptiveThresholdResponse;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 79, 1, 0);
            GetBandsAdaptiveThresholdResponse answer = new GetBandsAdaptiveThresholdResponse(header, new byte[] {});
            return answer;
        }
    }

    //Шифр 80
    public async Task<DefaultMessage> SetVoiceJammingSettings(int FrequencyKhz, byte DeviationCode, DspDataModel.RadioJam.VoiceJammingMode VoiceJammingMode,  string FilePath)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 80, length: 4 + 1 + 1 + 4 + 2 * FilePath.Length);
            byte[] message = SetVoiceJammingSettingsRequest.ToBinary(header, FrequencyKhz, DeviationCode, VoiceJammingMode, FilePath);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 80, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 81
    public async Task<DefaultMessage> SetVoiceJamming(bool IsJammingEnabled)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 81, length: 1);
            byte[] message = SetVoiceJammingRequest.ToBinary(header, IsJammingEnabled);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 81, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 82
    public async Task<DefaultMessage> SetTypeLoad(byte Liter, byte State)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 82, length: 2);
            byte[] message = SetShaperLiterStateRequest.ToBinary(header, Liter, State);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 82, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 83
    public async Task<DefaultMessage> SetPowerPercentage(byte Liter, byte PercentCode)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 83, length: 2);
            byte[] message = SetShaperLiterStateRequest.ToBinary(header, Liter, PercentCode);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 83, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 84*
    public async Task<DefaultMessage> SetSynchronizationSettings(short CycleSynchronizationShift, short TactingCycleLengthMs)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 84, length: 2 + 2);
            byte[] message = SetCycleSyncSettingsRequest.ToBinary(header, CycleSynchronizationShift, TactingCycleLengthMs);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 84, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

    //Шифр 85
    public async Task<DefaultMessage> SendPoyntingCommutatorCommand(DspDataModel.Hardware.CommutatorCommands commutatorCommand)
    {
        if (client == null)
            return null;
        if (client.Connected)
        {
            MessageHeader header = GetMessageHeader(code: 85, length: 1);
            byte[] message = SendPoyntingCommutatorCommandRequest.ToBinary(header, commutatorCommand);

            var taskCompletionSource = await SendRequest(header, message).ConfigureAwait(false);
            var answer = await taskCompletionSource.Task.ConfigureAwait(false) as DefaultMessage;
            return answer;
        }
        else
        {
            MessageHeader header = new MessageHeader(0, 0, 85, 1, 0);
            DefaultMessage answer = new DefaultMessage(header);
            return answer;
        }
    }

}
